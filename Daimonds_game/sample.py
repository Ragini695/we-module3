import pygame
import random

# Initialize Pygame
pygame.init()

# Set up the display (window)
screen_width, screen_height = 800, 600
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Card Game")

# Define colors
GREEN = (13, 168, 65)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

# Load font
font = pygame.font.Font(None, 36)

# Card class
class Card:
    def __init__(self, back_image, front_image, pos):
        self.back_image_original = pygame.image.load(back_image)
        self.front_image_original = pygame.image.load(front_image)

        self.back_image = pygame.transform.scale(self.back_image_original, (screen_width // 6, screen_height // 3))
        self.front_image = pygame.transform.scale(self.front_image_original, (screen_width // 6, screen_height // 3))
        
        self.pos = pos
        self.face_up = False

    def draw(self):
        if self.face_up:
            screen.blit(self.front_image, self.pos)
        else:
            screen.blit(self.back_image, self.pos)

    def flip(self):
        self.face_up = not self.face_up

# Deck class
class Deck:
    def __init__(self, card_paths):
        self.cards = []
        self.used_cards = []
        self.flipped_card = None  # Track the currently flipped card

        self.user_cards = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'j': 11, 'q': 12, 'k': 13, 'a': 14}
        self.computer_cards = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'j': 11, 'q': 12, 'k': 13, 'a': 14}

        self.score_user = 0
        self.score_computer = 0
        self.banker_cards = []
        for path in card_paths:
            card = Card(path[0], path[1], (100, 200))  # Adjust position as needed
            self.cards.append(card)
            self.banker_cards.append(path[1].split('_')[1][5:])
        self.banker_cards_values = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'jack': 11, 'queen': 12, 'king': 13, 'ace': 14}

        self.bid_input = ''
        self.bid_text = font.render('Your bid: ', True, BLACK)
        self.message = ''

    def draw(self):
        for card in self.cards:
            card.draw()
        for i, card in enumerate(self.used_cards):
            card.draw()
            card.pos = (600, 50 + i * 40)

        # Display bid value and scores
        bid_text_surface = font.render("Bid Value: " + self.bid_input, True, BLACK)
        screen.blit(bid_text_surface, (20, 20))

        score_text = font.render("Scores:", True, BLACK)
        screen.blit(score_text, (20, 60))

        user_score_text = font.render("User: {}".format(self.score_user), True, BLACK)
        screen.blit(user_score_text, (20, 100))

        computer_score_text = font.render("Computer: {}".format(self.score_computer), True, BLACK)
        screen.blit(computer_score_text, (20, 140))

        # Display message
        message_surface = font.render(self.message, True, BLACK)
        screen.blit(message_surface, (20, 180))

    def handle_click(self, pos):
        # Check if there's a flipped card and if it was clicked
        if self.flipped_card and self.flipped_card.pos[0] <= pos[0] <= self.flipped_card.pos[0] + self.flipped_card.front_image.get_width() and \
                self.flipped_card.pos[1] <= pos[1] <= self.flipped_card.pos[1] + self.flipped_card.front_image.get_height():
            self.bid()
            
        else:
            for card in reversed(self.cards):
                if card.face_up:
                    continue  # Skip face-up cards
                if card.pos[0] <= pos[0] <= card.pos[0] + card.front_image.get_width() and \
                        card.pos[1] <= pos[1] <= card.pos[1] + card.front_image.get_height():
                    card.flip()
                    self.flipped_card = card  # Set flipped_card to the clicked card
                    break

    def handle_keypress(self, key):
        if key == pygame.K_RETURN:
            if self.bid_input in self.user_cards:
                self.bid()
            else:
                self.message = "Invalid bid. Please enter a valid bid."
            self.bid_input = ''
            self.bid_text = font.render('Your bid: ', True, BLACK)
        elif key == pygame.K_BACKSPACE:
            self.bid_input = self.bid_input[:-1]
        else:
            self.bid_input += pygame.key.name(key)

    def bid(self):
        user_choice = self.bid_input
        if user_choice in self.user_cards:
            computer_choice = random.choice(list(self.computer_cards.keys()))
            self.message = 'Computer bidded with: {}'.format(computer_choice)
            if self.user_cards[user_choice] > self.computer_cards[computer_choice]:
                self.score_user += self.banker_cards_values[self.banker_cards[-1]]
                self.banker_cards.pop(-1)
            elif self.user_cards[user_choice] < self.computer_cards[computer_choice]:
                self.score_computer += self.banker_cards_values[self.banker_cards[-1]]
                self.banker_cards.pop(-1)
            else:
                score = self.banker_cards_values[self.banker_cards[-1]]
                self.score_computer += score // 2
                self.score_user += score // 2
                self.banker_cards.pop(-1)
            del self.user_cards[user_choice]
            del self.computer_cards[computer_choice]
            if len(self.used_cards) < 13:
                self.used_cards.append(self.flipped_card)
                self.flipped_card = None
                self.bid_input = ''
        if len(self.used_cards) == 13:
            if self.score_computer > self.score_user:
                self.message = "You lost!!!!"
            elif self.score_computer < self.score_user:
                self.message = "You Won!!!!"
            else:
                self.message = "It's a Tie!!!!"

# Create 
card_paths = [
    ("card_imgs/backpattern_img.jpg","card_imgs/2_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/3_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/4_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/5_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/6_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/7_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/8_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/9_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/10_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/jack_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/queen_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/king_of_diamonds.png"),
    ("card_imgs/backpattern_img.jpg","card_imgs/ace_of_diamonds.png"),
]
random.shuffle(card_paths)
deck = Deck(card_paths)  # Adjust number of cards as needed

# Main game loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # Left mouse button
                deck.handle_click(pygame.mouse.get_pos())
        elif event.type == pygame.KEYDOWN:
            deck.handle_keypress(event.key)

    screen.fill(GREEN)
    deck.draw()
    pygame.display.flip()

pygame.quit()
